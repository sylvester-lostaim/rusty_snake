# A Rusty Snake
- Just for fun where written in rust.
- A ncurses terminal-based traditional snake game.
- Max score is fixed to 100.

# hmm...
- I guess rust's ncurses is stable enough?

# Requirement
- Only tested on Linux. (maybe just works on Windows out-of-the-box)
- Low level ncurses is enough.

# Installation
- Ensure you have rust cargo installed.
- Clone the project, change into the directory and compile it.
```
cd rusty snake
cargo run          # it will auto-build if not yet built
```

# Interface
## Welcome Page
![Welcome Page](images/welcome_page.png)
## In-game Page
![In-game Page](images/in_game.png)
