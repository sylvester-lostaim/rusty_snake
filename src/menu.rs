pub mod menu_page {

  extern crate ncurses;
  use ncurses::*;
  use crate::values::keypress;

  pub fn paint_page() -> bool {
    let instruction = "Rusty Snake Game 

TERMINAL SIZE / PAINTING AREA
x-axis                     148
y-axis                     35

ACTION                     KEY
Move to top                Arrow Up
Move to bottom             Arrow Down
Move to left               Arrow Left
Move to right              Arrow Right
Quit                       q

Press \"Enter\" to start ...";
    addstr(instruction);
    refresh();
    return user_response();
  }

  fn user_response() -> bool {
    loop {
      let response = getch();
      if response == keypress::KEY_ENTER {
        return true;
      } else if response == keypress::KEY_Q {
        return false;
      }
    }
  }
}
