extern crate ncurses;
use ncurses::*;

mod values;
mod menu;
use crate::menu::menu_page;
mod game;
use crate::game::game_page;

fn main() {
  initscr();                                        // initialize ncurse
  resizeterm(35, 148);                              // fixed painting area
  curs_set(CURSOR_VISIBILITY::CURSOR_INVISIBLE);    // hide cursor
  noecho();                                         // disable keypress feedback
  raw();                                            // dont process signal

  // launch menu page
  if menu_page::paint_page() {
    // if true, user starts game
    if ! game_page::paint_page() {
      // if false, user quits program
      endwin();
    }
  } else {
    // if false, user quits program
    endwin();
  }
}
