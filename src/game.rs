pub mod game_page {

  extern crate ncurses;
  use ncurses::*;
  use std::thread::sleep;
  use std::time::Duration;
  use rand::Rng;
  use std::process::exit;
  use crate::values::keypress;
  use crate::values::gameblock;

  // initialize game
  pub fn paint_page() -> bool {
    // cleanup page
    erase();
    
    // title info
    mvaddstr(0, 0, "SCORE: 0");
    mvaddstr(0, 134, "SPEED: 0.3 sec");

    // draw gate 
    // painting area : 35(y) * 148(x)
    // horizontal
    for x in 1..146 { mvaddch(1, x, gameblock::GATE); }
    for x in 1..146 { mvaddch(33, x, gameblock::GATE); }
    // vertical
    for y in 2..33 { mvaddch(y, 1, gameblock::GATE); }
    for y in 2..33 { mvaddch(y, 145, gameblock::GATE); }

    // draw snake
    mvaddch(17, 71, gameblock::SNAKE_HEAD);
    for s in 72..75 { mvaddch(17, s, gameblock::SNAKE_BODY); }

    return user_response();
  }

  fn user_response() -> bool {
    // initialize snake position, direction, speed, score
    let speed = 300;
    let mut direction = "LEFT";
    let mut snake = vec![
      vec![17, 72], 
      vec![17, 73], 
      vec![17, 74], 
      vec![17, 75]
    ];
    let mut score: i32 = 0;
    let mut keyspam = false; // keyspam blocker
    place_food();

    timeout(1);     // accept user response every 1ms
    loop {
      let response = getch();
      if response == keypress::KEY_Q {
        return false;

      // dont allow movement to happen in reverse direction
      } else if response == keypress::KEY_UP {
        if direction != "DOWN" { if ! keyspam { direction = "UP"; keyspam = true; } }

      } else if response == keypress::KEY_DOWN {
        if direction != "UP" { if ! keyspam { direction = "DOWN"; keyspam = true; } }

      } else if response == keypress::KEY_LEFT {
        if direction != "RIGHT" { if ! keyspam { direction = "LEFT"; keyspam = true; } }

      } else if response == keypress::KEY_RIGHT {
        if direction != "LEFT" { if ! keyspam { direction = "RIGHT"; keyspam = true; } }

      } else if response == -1 {
        // add head to new area
        let mut new_head = vec![vec![0, 0]];
        if direction == "UP" { 
          new_head[0][0] = snake[0][0] - 1;
          new_head[0][1] = snake[0][1];

        } else if direction == "DOWN" {
          new_head[0][0] = snake[0][0] + 1;
          new_head[0][1] = snake[0][1];

        } else if direction == "LEFT" {
          new_head[0][0] = snake[0][0];
          new_head[0][1] = snake[0][1] - 1;

        } else if direction == "RIGHT" {
          new_head[0][0] = snake[0][0];
          new_head[0][1] = snake[0][1] + 1;
        }

        // get new score and check snake whether still alive
        let new_score = snake_alive(&new_head, score);
        if new_score > score {
          score = new_score;
        } else {
            // remove tail
            // old tail for growth, if food consumed
            mvaddch(snake[snake.len()-1][0], snake[snake.len()-1][1], gameblock::EMPTY);
            snake.pop();
        }

        // change head to body
        mvaddch(snake[0][0], snake[0][1], gameblock::SNAKE_BODY);

        // update snake drawing coordinates, draw new head
        new_head.extend(snake);
        snake = new_head;
        mvaddch(snake[0][0], snake[0][1], gameblock::SNAKE_HEAD);

        // consume keypress, delay movement
        keyspam = false;
        sleep(Duration::from_millis(speed));
      }
    }
  }

  fn place_food() {
    let mut x = 0;
    let mut y = 0;

    // randomly pick coordinate which is an EMPTY gameblock inside gate
    while mvinch(y, x) != gameblock::EMPTY {
      x = rand::thread_rng().gen_range(2..145);
      y = rand::thread_rng().gen_range(2..32);
    }

    // place food
    mvaddch(y, x, gameblock::FOOD);
  }

  // check snake condition to be alive
  fn snake_alive(new_head: &Vec<Vec<i32>>, mut score: i32) -> i32 {
    let next_block = mvinch(new_head[0][0], new_head[0][1]);

    // if next block is gate or snake body 
    if next_block == gameblock::GATE || next_block == gameblock::SNAKE_BODY {
      end_game(false);        // false == bad end game

    // if next block is food
    } else if next_block == gameblock::FOOD {
      // add score, grow tail, place food again
      score = score + 1;
      mvaddstr(0, 7, &score.to_string());
      place_food();

      if score > 99 {
        end_game(true);       // true = good end game
      }
    }
    return score;
  }

  // end game
  fn end_game(state: bool) {
    if state { 
      mvaddstr(0, 7, "100");
      mvaddstr(34, 0, "100! Press \"Q\" to quit...");
    } else { 
      mvaddstr(34, 0, "OOPS! Press \"Q\" to quit...");
    }
    timeout(-1);
    let response = getch();
    if response == keypress::KEY_Q {
        endwin();
        exit(0);
    }
  }
}
