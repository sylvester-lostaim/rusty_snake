pub mod keypress {
  pub const KEY_ENTER: i32 = 10; 

  pub const KEY_UP: i32 = 65;
  pub const KEY_DOWN: i32 = 66;
  pub const KEY_RIGHT: i32 = 67;
  pub const KEY_LEFT: i32 = 68;

  pub const KEY_Q: i32 = 113;
}

pub mod gameblock {
  pub const SNAKE_HEAD: u32 = 35;
  pub const SNAKE_BODY: u32 = 64;
  pub const GATE: u32 = 43;
  pub const EMPTY: u32 = 32;
  pub const FOOD: u32 = 83;
}
